<html>
<style>
  th {
    font-weight: bold;
  }
</style>
<body>
<table>
  <tr>
    <td colspan="10">
      <h1>
        Exported at {{ \Carbon\Carbon::now()->format('Y-m-d H:s:i') }}
      </h1>
    </td>
  </tr>
  <tr>
    <th>ID</th>
    <th>Name</th>
    <th>Price</th>
  </tr>
  @foreach($products as $product)
    <tr>
      <td>{{ $product->id }}</td>
      <td>{{ $product->name }}</td>
      <td align="right">{{ str_replace('$', '', $product->price) }}</td>
    </tr>
  @endforeach
</table>
</body>
</html>