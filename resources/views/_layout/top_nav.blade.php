<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
              aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Coding Shop House</a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">
        <li>
          <a>
            @if(Auth::check())
              {{ Auth::user()->name }}
            @endif
          </a>
        </li>
        <li>
          @if(Auth::check())
            <form method="POST" action="{{ url('logout') }}" accept-charset="UTF-8" name="logout-form" id="logout-form">
              {{ csrf_field() }}
              <button style="margin-top: 8px; margin-right: 8px;" type="submit" class="btn btn-default">Log Out</button>
            </form>
          @else
            <a style="padding: 8px; margin-top: 6px; margin-right: 6px;min-width: 70px;" href="{{ url('login') }}"
               class="btn btn-default">Login</a>
          @endif
        </li>
      </ul>
    </div>
  </div>
</nav>