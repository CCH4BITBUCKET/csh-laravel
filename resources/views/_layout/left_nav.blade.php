<div class="col-sm-3 col-md-2 sidebar">
  <ul class="nav nav-sidebar">
    <li class="@if($pageActive == 'user') active @endif">
      <a href="/backend/users">Users</a>
    </li>
    <li class="@if($pageActive == 'product') active @endif">
      <a href="/backend/products">Products</a>
    </li>
    <li class="@if($pageActive == 'email') active @endif">
      <a href="/backend/email">Send Email</a>
    </li>
  </ul>
</div>