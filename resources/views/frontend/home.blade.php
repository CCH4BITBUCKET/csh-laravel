@extends('_layout.base_frontend')

@section('css')
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
  <!-- Add the slick-theme.css if you want default styling -->
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick-theme.css"/>
  <style>
    .product-image {
      background-size: cover !important;
      background-position: center !important;
      width: 100%;
      min-height: 150px;
    }

    .overlay {
      opacity: 0.3;
      background-color: grey;
      position: absolute;
      min-height: 50px;
      width: 375px;
      bottom: 0px;
    }

    .product-name {
      position: absolute;
      bottom: 15px;
      right: 30px;
      color: white;
    }
  </style>
@endsection

@section('content')
  <div class="carousel">
    @foreach($images as $image)
      <div>
        <img src="{{$image->thumb_url}}">
      </div>
    @endforeach
  </div>
  <div class="row main">
    <p>
      Nulla porttitor accumsan tincidunt. Sed porttitor lectus nibh. Vivamus suscipit tortor eget felis porttitor
      volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porttitor accumsan tincidunt. Quisque
      velit nisi, pretium ut lacinia in, elementum id enim. Quisque velit nisi, pretium ut lacinia in, elementum id
      enim. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Sed porttitor lectus nibh. Lorem ipsum
      dolor sit amet, consectetur adipiscing elit.
    </p>
    <p>
      Vivamus suscipit tortor eget felis porttitor volutpat. Donec rutrum congue leo eget malesuada. Praesent sapien
      massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat.
      Curabitur aliquet quam id dui posuere blandit. Vivamus suscipit tortor eget felis porttitor volutpat. Praesent
      sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur aliquet quam id dui posuere blandit. Cras
      ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in, elementum id enim.
    </p>
  </div>
  <div class="row main">
    @foreach($products as $product)
      <div class="col-md-4" style="margin-bottom: 15px;">
        <div class="product-image" style="background-image: url('{{url($product->images[0]->url)}}')">
          <div class="overlay">
          </div>
          <div class="product-name">
            {{ $product->name }} {{$product->price}}
          </div>
        </div>
      </div>
    @endforeach
  </div>
@endsection

@section('js')
  <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
  <script>
      $(document).ready(function () {
          $('.carousel').slick({
              dots: true,
              infinite: true,
              speed: 300,
              slidesToShow: 1,
              centerMode: true,
              variableWidth: true,
              autoplay: true,
              autoplaySpeed: 1000,

          });
      });
  </script>
@endsection