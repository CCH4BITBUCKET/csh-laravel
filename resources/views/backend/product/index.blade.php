@extends('_layout.base_backend')

@section('css')
  <link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
  <div class="page-header">
    <span class="h1">
      Product
    </span>
    <div class="pull-right">
      <form class="hidden" id="form-export-pdf" method="post" action="{{ url('backend/product/export-pdf') }}">
        {{csrf_field()}}
      </form>
      <form id="form-export" method="post" action="{{ url('backend/product/export') }}">
        {{csrf_field()}}
        <a class="btn btn-primary" href="{{ url('backend/product/create') }}">
          <i class="glyphicon glyphicon-plus"></i> &nbspCreate
        </a>
        <button type="submit" id="btn-export" class="btn btn-primary">
          <i class="glyphicon glyphicon-export"></i> &nbspExport
        </button>
        <button type="button" id="btn-export-pdf" class="btn btn-primary">
          <i class="glyphicon glyphicon-export"></i> &nbspExport PDF
        </button>
      </form>
    </div>

  </div>

  <table id="product-table" class="table table-hover" style="width: 100%;">
    <thead>
    <tr>
      <th>Name</th>
      <th>Price</th>
      <th>Created By</th>
      {{--<th></th>--}}
    </tr>
    </thead>

    <tbody>
    @foreach($products as $product)
      <tr id="{{$product->id}}" class="edit" style="cursor: pointer;">
        <td>{{$product->name}}</td>
        <td>{{$product->price}}</td>
        <td>{{$product->createdBy->name}}</td>
        {{--<td>--}}
        {{--<a href="{{ url('backend/product', $product->id . "/edit") }}">--}}
        {{--<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>--}}
        {{--</a>--}}
        {{--</td>--}}
      </tr>
    @endforeach
    </tbody>
    <tfoot>

    </tfoot>
  </table>

@endsection

@section('js')
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
  <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
  <script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
  <script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
  <script>
      $(document).ready(function () {
//          $('table').dataTable();
          $("#btn-export-pdf").click(function(){
             $("#form-export-pdf").submit();
          });
//          $('table').dataTable({
//              dom: 'Bfrtip',
//              buttons: [
//                  'copy', 'csv', 'excel', 'pdf', 'print'
//              ],
//              "aaSorting": [],
//              "aoColumnDefs": [
//                  {"bSortable": false, "aTargets": [2]}
//              ]
//          });
          $('#product-table').DataTable({
              processing: true,
              aaSorting: [],
              scrollX: true,
              language: {
                  processing: "<span>Processing</span>",
              },
              serverSide: true,
              ajax: {
                  url: '/backend/a/products',
              },
              searching: false,
              ordering: true,
              columns: [
                  {data: 'name'},
                  {data: 'price'},
                  {data: 'user_name'}
              ]
          });
      });

      $(document).on("click", "tr.edit", function () {
          var productId = $(this).attr("id");
          window.location.href = "{{ url('backend/product') }}/" + productId + "/edit";
      });
  </script>
@endsection