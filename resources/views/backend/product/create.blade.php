@extends('_layout.base_backend')

@section('title')
  Create Product
@endsection

@section('content')

  <h1 class="page-header">Create New Product</h1>

  <form action="{{ url('backend/product/create') }}" method="post">
    {{ csrf_field() }}
    <div class="form-group">
      <label for="name">Name</label>
      <input type="text" name="name" class="form-control" id="name" placeholder="Name" value="{{old('name')}}">
    </div>
    <div class="form-group">
      <label for="price">Price</label>
      <input type="text" name="price" class="form-control" id="price" placeholder="0.00"
             value="{{old('price')}}">
    </div>

    <button type="submit" class="btn btn-primary">Create</button>
  </form>

@endsection

@section('js')
  <script>
      $(document).ready(function () {

      });
  </script>
@endsection