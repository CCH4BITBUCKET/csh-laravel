@extends('_layout.base_backend')

@section('content')
  <form method="post" action="{{url('backend/send-email')}}">
    {{csrf_field()}}
    <textarea class="form-control" name="enquiry"></textarea>
    <br>
    <button type="submit" class="btn btn-primary">Send Email</button>
  </form>
@endsection