### Export

In this lesson, we will look into how we can export our data out to in excel, csv and pdf format.

#### The What You See Is What You Export way

- Utilize datatable built in export

- Open `resources/views/backend/product/index.blade.php`. 

     - Add this after `extends()`

```blade
@section('css')
  <link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet">
@endsection
```
  
   - Add this at the first line of `@section('js')`

```blade
<script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
  <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
  <script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
  <script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
```

- Change your datatable initialization to below:

```blade
$('table').dataTable({
  dom: 'Bfrtip',
  buttons: [
      'copy', 'csv', 'excel', 'pdf', 'print'
  ],
  "aaSorting": [],
  "aoColumnDefs": [
      {"bSortable": false, "aTargets": [2]}
  ]
});
```
---

#### The highly customizable way

- Add a new route in `routes/web.php`.

`Route::post('backend/product/export', 'ProductController@export');`

- Add a new button in your product listing view.
 Open `resources/views/backend/product/index.blade.php`, replace the create button html with below:
 
```blade
<form id="form-export" method="post" action="{{ url('backend/product/export') }}">
    {{csrf_field()}}
    <a class="btn btn-primary" href="{{ url('backend/product/create') }}">
      <i class="glyphicon glyphicon-plus"></i> &nbspCreate
    </a>
    <button type="submit" id="btn-export" class="btn btn-primary" >
      <i class="glyphicon glyphicon-export"></i> &nbspExport
    </button>
</form>
```

- Let's add a [package (Laravel Excel)](http://www.maatwebsite.nl/laravel-excel/docs/getting-started#installation) to help us with this, execute the command in your terminal:

`composer require maatwebsite/excel ~2.1.0`

- Add the provider into your `config/app.php`'s providers and alias array respectively.

```php
//Add to provider array
Maatwebsite\Excel\ExcelServiceProvider::class,

//Add to alias array
'Excel' => Maatwebsite\Excel\Facades\Excel::class,
```

Now the package is in and all setup, now let's head over to `app/Http/Controllers/ProductController.php` and create the `export()` method. 

```php
public function export()
{
    $fileName = Carbon::now()->format('Y-m-d') . '_products';
    \Excel::create($fileName, function ($excel) {
        $excel->sheet('Sheet 1', function ($sheet) {
            $sheet->loadView('exports.products', ['products' =>Product::get()]);
        });
    })->download('xlsx'); // xls, xlsx, csv    
}
```

In order to export to pdf, you would beed another package.

`composer require dompdf/dompdf ~0.6.1`

Then you can do `->download('pdf')`.

---

Reference:

- [Laravel Excel](http://www.maatwebsite.nl/laravel-excel/docs/getting-started)

- [Datatables.net](https://datatables.net/)





