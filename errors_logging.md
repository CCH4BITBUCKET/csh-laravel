### Errors & Logging

Out of the box, error and exception handling is already configured for you in a new Laravel project. 

`app\Exception\Handler` class is the main class that handles the exceptions that are triggered by your application.

2 type of logs in general:
- system logs, you can define to have the logs in anywhere you want in your server (See example below).
- laravel logs, located at `storage/logs/laravel.log`

Example of system logs in nginx virtual hosts
```
server {
    listen       8143;
    server_name  localhost;
    root       /var/www/awesome-blog/public;
    client_max_body_size 24M;
    charset utf-8;
    client_body_temp_path /tmp/nginx/;
 
    access_log  /usr/local/etc/nginx/logs/awesome-blog.access.log  main;
    error_log /usr/local/etc/nginx/logs/awesome-blog.error.log;

    location / {
        index index.html index.htm index.php;
        try_files $uri $uri/ /index.php?$query_string;
        # include   /usr/local/etc/nginx/conf.d/php-fpm;
    }

    if (!-d $request_filename) {
       rewrite     ^/(.+)/$ /$1 permanent;
    }

    location ~* \.php$ {
       fastcgi_pass 127.0.0.1:9000;
       include fastcgi.conf;
    }
}
```
---

#### Configuration

In `config/app.php`, the `debug` option determines how much information about an error is actually displayed to the user. It actually retrieves value from `APP_DEBUG` from your `.env` file.

For local development, you should set `APP_DEBUG` to `true`, where else if you set `true` in production, you might risk exposing sensitive information to the users.
 
 For example, let's see how we can 'accidentally' leaks out important data. if you open your `.env` and update `DB_PASSWORD` to a wrong password and `APP_DEBUG` flag is `true`, once you refresh your page on your browser, it will shows your database name, username and tables on the browser.

---

#### Log Storage

In `config/app.php`, you can set how you want the log files to be stored.

```php
 /*
    |--------------------------------------------------------------------------
    | Logging Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log settings for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Settings: "single", "daily", "syslog", "errorlog"
    |
    */

    'log' => env('APP_LOG', 'single'),

    'log_level' => env('APP_LOG_LEVEL', 'debug'),
```
---

We can also store our logs to an external service for better management and notifications. Let's setup [Bugsnag](https://bugsnag.com/):

- Register an account, the free plan does has some limitations but it's usually sufficient for small projects. 
- [Install](https://docs.bugsnag.com/platforms/php/laravel/) the Bugsnag package
    - In your terminal , `composer require "bugsnag/bugsnag-laravel:^2.0"`
    - In `app/config.php`, add to your `providers` array, and `aliases` array.
```
'providers' => [
    // ...
    Bugsnag\BugsnagLaravel\BugsnagServiceProvider::class,
    // ...
],
'aliases' => [
    // ...
    'Bugsnag' => Bugsnag\BugsnagLaravel\Facades\Bugsnag::class,
],
```

Add your api key to `.env`.

`BUGSNAG_API_KEY=your-api-key-here`

Now you can send data to Bugsnag with the follow:

```php
try {
    // Some potentially crashy code
} catch (Exception $ex) {
    Bugsnag::notifyException($ex);
}
```

