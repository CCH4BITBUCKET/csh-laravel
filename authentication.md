### Authentication ###

- As usual, configuration files will be inside the `config` directory. Open `config/auth.php` and have a look at the available configs.

- Laravel's default authentication is made up of `guards` and `providers`. 

  - `guards` - define how users are authenticated for each request, Laravel comes with a `session` guard which maintain state using session and cookies. HTTP is a stateless protocol, the connection between browser and server is lost once the transaction ends, in order to maintain the <i>state</i>, we can use cookies to do that.
  
  - `providers` - define how users are retrieved from your persistent storage, which most likely will be from your database. 

It might sounds confusing as of now, but most likely you wouldn't need to modify the default authentication configuration.

```php
'guards' => [
    'web' => [
        'driver' => 'session',
        'provider' => 'users',
    ],

    'api' => [
        'driver' => 'token',
        'provider' => 'users',
    ],
],
```

Laravel comes with 2 `guards` predefined, `web` guard is using the driver session which is our typical login process. `api` guard is using driver token which is great for working with APIs.

```php
'providers' => [
    'users' => [
        'driver' => 'eloquent',
        'model' => App\Models\User::class,
    ],
],
```

Inside the `providers` array, we will be using Laravel's ORM eloquent to check against our users table, update it from `App\User::class` to `App\Models\User::class`.

---

Now let's execute this command in your terminal to let Laravel bootstraps the necessary files for authentication.

`php artisan make:auth`

Laravel comes with pre-built authentication controllers, which can be found at `App\Http\Controllers\Auth\` namespace.

- `RegisterController` - handles new user registration
- `LoginController` - handles authentication
- `ForgotPasswordController` - handles emailing links to reset passwords
- `ResetPasswordController` - contains logic to reset passwords

---

### Routes ###

After running `make:auth`, `Auth::routes();` is being placed into your `routes/web.php` route file. 

The class `Auth` is actually a facade used by Laravel, declared in `config/app.php` at the `$aliases` array.

`'Auth' => Illuminate\Support\Facades\Auth::class,`

The class for the `Auth` facade actually resides in `Illuminate\Routing\Router.php`, inside there's a method `auth()` which adds the following routes to our own route file.

```php
public function auth()
{
    // Authentication Routes...
    $this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
    $this->post('login', 'Auth\LoginController@login');
    $this->post('logout', 'Auth\LoginController@logout')->name('logout');

    // Registration Routes...
    $this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    $this->post('register', 'Auth\RegisterController@register');

    // Password Reset Routes...
    $this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    $this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    $this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    $this->post('password/reset', 'Auth\ResetPasswordController@reset');
}
```
--- 

### Protecting Routes ###

You can protect your routes to only allow authenticated users to access.

```php
Route::get('profile', function () {
    // Only authenticated users may enter...
})->middleware('auth');
```

Now, let's open our `routes/web.php` and update our routes as below, we will also prefix the word `backend` to our route and all this routes can only be accessible by authenticated users.

```php
Route::group(['prefix' => 'backend', 'middleware' => 'auth'], function(){
    // Prefix with /backend/ and only authenticated users may enter
    // ...
    // ...
});
```

--- 

### Views ###

After running `make:auth`, `resources/views/layouts/` will be created and contain a sample base layout for your application. It is a good starting point for us to use and you can of cause customize it however you want.
 
  - Change `layouts/app.blade.php` to `auth.blade.php`
     and move to `_layout` folder.
  - Update `assets()` to `mix()`.
  - Update below views to extends from `_layout/auth.blade.php` 
      - `auth/login.blade.php` 
      - `auth/register.blade.php`
      - `home.blade.php`
  - Update `Middleware/RedirectIfAuthenticated` default route
 ---
 
 In `LoginController`, `RegisterController` and `ResetPasswordController`, you can customize where the user is being redirect to after they login / register or reset their password.
 
 `protected $redirectTo = ''`
 
 If you need custom logic to determine the redirect url. You can override the `redirectTo()` method in your controller. 
 
 ```php
 protected function redirectTo()
 {
     return '/path';
 }
 ```
 
 Inside `RegisterController`, you are free to modify the 2 methods available should you need more customization.
 
 `validator()` - Logic to check for mandatory fields and validation
 `create()` - Create user in the database
 
---

### Retrieving Authenticated User ###

You may access the authenticated user via the Auth facade anywhere in your project.

```php
use Illuminate\Support\Facades\Auth;

// Get the currently authenticated user...
$user = Auth::user();

// Get the currently authenticated user's ID...
$id = Auth::id();

```

---

### Check if user is authenticated ###

```php
use Illuminate\Support\Facades\Auth;

if (Auth::check()) {
    // The user is logged in...
}
```
---

### Log Out ###

Modify top nav to include log out buttons.

Open `resources/views/_layout/top_nav.blade.php`
```blade
<div id="navbar" class="navbar-collapse collapse">
  <ul class="nav navbar-nav navbar-right">
    <li>
      <a>{{ Auth::user()->name }}</a>
    </li>
    <li>
      <form method="POST" action="{{ url('logout') }}" accept-charset="UTF-8" name="logout-form" id="logout-form">
        {{ csrf_field() }}
        <button style="margin-top: 8px; margin-right: 8px;" type="submit" class="btn btn-default">Log Out</button>
      </form>
    </li>
  </ul>
</div>
```

Or if you want to manually log user out, you can just call the following code in your project.
```php
Auth::logout();
```

---

