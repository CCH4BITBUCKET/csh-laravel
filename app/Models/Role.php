<?php
/**
 * Created by PhpStorm.
 * User: jctan
 * Date: 22/04/2017
 * Time: 11:29
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public $timestamps = false;
}