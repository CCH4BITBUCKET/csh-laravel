<?php
/**
 * Created by PhpStorm.
 * User: jctan
 * Date: 08/04/2017
 * Time: 00:23
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $fillable = [
        'name',
        'price'
    ];

    public function images()
    {
        return $this->hasMany('App\Models\ProductImage');
    }

    //Relationship
    public function createdBy()
    {
        return $this->belongsTo('App\Models\User', 'created_by')->withTrashed();
    }

    // Accessor
    public function getPriceAttribute($price)
    {
        return '$' . number_format($price, 2);
    }

    // Mutator
    public function setPriceAttribute($price)
    {
        $this->attributes['price'] = str_replace('$', '', $price);
        $this->attributes['price'] = str_replace(',', '', $this->attributes['price']);
    }
    
    //Scope
    public function scopeExpensive($query)
    {
        return $query->where('price', '>', 1000);
    }


}