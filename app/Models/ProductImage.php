<?php
/**
 * Created by PhpStorm.
 * User: jctan
 * Date: 08/04/2017
 * Time: 00:23
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    public $table = 'product_images';
    public $timestamps = false;
}