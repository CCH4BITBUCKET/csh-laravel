<?php
/**
 * Created by PhpStorm.
 * User: jctan
 * Date: 27/04/2017
 * Time: 20:35
 */

namespace App\Service;


use App\Models\Product;

class CheckProduct
{
    public function retrieve($productId = null)
    {
        $products = new Product;
        if ($productId) {
            $products = $products->where('id', $productId);
        }
        $products = $products->get(['name', 'price']);

        return $products;
    }
}