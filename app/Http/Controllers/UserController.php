<?php
/**
 * Created by PhpStorm.
 * User: jctan
 * Date: 08/04/2017
 * Time: 00:03
 */

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        \View::share(['pageActive' => 'user']);
    }

    public function index()
    {
        $users = User::get();

        return view('backend.user.index', ['users' => $users]);
    }

    public function create()
    {
        return view('backend.user.create');
    }

    public function store(CreateUserRequest $request)
    {
//        $validator = \Validator::make($request->all(), [
//            'name'     => 'required',
//            'email'    => 'required|unique:users',
//            'password' => 'required|confirmed|min:6',
//        ]);
//
//        if ($validator->fails()) {
//            return back()
//                ->withErrors($validator)
//                ->withInput();
//        }

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = \Hash::make($request->password);
        $user->save();

        flash()->success('User ' . $user->name . ' created');

        return redirect('backend/users');
    }

    public function edit($id)
    {
        try {
            $user = User::findOrFail($id);
        } catch (\Exception $e) {
            flash()->error('Invalid ID');

            return redirect('backend/users');
        }

        return view('backend.user.edit', ['user' => $user]);
    }

    public function update(Request $request)
    {
        $user = User::find($request->id);
        $user->update($request->only('name', 'email'));

        flash()->success('User updated successfully.');

        return redirect('backend/users');
    }

    public function delete(Request $request)
    {
        try {
            $user = User::findOrFail($request->id);
            $user->delete();
        }catch(\Exception $exception){
            flash()->error($exception->getMessage());
        }

        flash()->success('User removed.');

        return redirect('backend/users');
    }

}