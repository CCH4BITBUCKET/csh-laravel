<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = ProductImage::get()->take(5);
        $products = Product::has('images')->with('images')->get()->take(9);
//dd($products->first()->images[0]->url);
        return view('frontend.home', compact('images', 'products'));
    }

    public function chromeDev()
    {
        return view('frontend.chrome_dev');
    }
}
