<?php

namespace App\Http\Controllers;

use App\Mail\ContactUsEmail;
use Illuminate\Http\Request;

class EmailController extends Controller
{
    /**
     * EmailController constructor.
     */
    public function __construct()
    {
        \View::share(['pageActive' => 'email']);
    }

    public function index()
    {
        return view('backend.test_email.index');
    }

    public function send(Request $request)
    {
        $enquiry = $request->enquiry;

        \Mail::to('omegachien@gmail.com')
//            ->cc('jc@greenroom.com.my')
//            ->bcc('jc@mailinator.com')
            ->queue(new ContactUsEmail($enquiry));

    }
}
