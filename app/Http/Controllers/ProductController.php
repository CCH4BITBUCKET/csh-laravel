<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductImage;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Response;
use Storage;

class ProductController extends Controller
{
    /**
     * ProductController constructor.
     */
    public function __construct()
    {
        \View::share(['pageActive' => 'product']);
    }

    public function index()
    {
//        $products = Product::select('products.*')
//            ->with('createdBy')
//            ->get();
        $products = [];

        return view('backend.product.index', compact('products'));
    }

    public function ajaxRetrieve()
    {
        $products = Product::select('products.id', 'products.name', 'products.price', 'users.name as user_name')
            ->join('users', 'users.id', '=', 'products.created_by')
            ->orderBy('products.name');

        $dataTable = \Datatables::of($products)
            ->setRowId(function ($product) {
                return $product->id;
            })
            ->setRowClass("edit pointer")
            ->make(true);

        return $dataTable;
    }

    public function create()
    {
        return view('backend.product.create');
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name'  => 'required',
            'price' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $product = Product::create($request->only('name', 'price'));
        flash()->success($product->name . ' created');

        return redirect('backend/products');
    }

    public function edit($id)
    {
        try {
            $product = Product::with('images')->findOrFail($id);
        } catch (Exception $e) {
            flash()->error('Product not found.');

            return redirect('backend/products');
        }

        return view('backend.product.edit', compact('product'));
    }

    public function update(Request $request)
    {
        $product = Product::findOrFail($request->id);
        $product->update($request->only('name', 'price'));

        flash()->success('Product updated.');

        return redirect('backend/products');
    }

    public function delete(Request $request)
    {
        $product = Product::findOrFail($request->id);
        $product->delete();

        flash()->success('Product removed.');

        return redirect('backend/products');
    }

    public function upload(Request $request)
    {
        $image = $request->image;
        $fileName = time() . "_" . $image->getClientOriginalName();

        //Store Image
        $image->storeAs('product_images', $fileName);

        //Thumbnail
        $thumbFileName = 'product_images/thumbnail/' . $fileName;
        $thumb = \Image::make($image->getRealPath())->resize(500, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $thumb->save($thumbFileName);

        //Save database
        $productImage = new ProductImage;
        $productImage->url = 'product_images/' . $fileName;
        $productImage->thumb_url = $thumbFileName;
        $productImage->product_id = $request->product_id;
        $productImage->save();

        return Response::json([
            'product_id' => $productImage->id,
            'url'        => $productImage->url,
            'thumb_url'  => $productImage->thumb_url,
        ]);
    }

    public function imageDelete(Request $request)
    {
        $imageId = $request->image_id;
        try {
            $productImage = ProductImage::findOrFail($imageId);

            Storage::delete($productImage->url);
            Storage::delete($productImage->thumb_url);

            $productImage->delete();

            return Response::json(['message' => 'success'], 200);
        } catch (Exception $e) {
            return Response::json(['message' => 'Invalid ID'], 400);
        }
    }

    public function export()
    {
        $products = Product::take(10)->get();

        $fileName = Carbon::now()->format('Y-m-d') . '_products';
        \Excel::create($fileName, function ($excel) use ($products){
            $excel->sheet('Sheet 1', function ($sheet) use ($products) {
                $sheet->loadView('exports.products', ['products' => $products]);
            });
        })->download('xlsx');
    }
}
