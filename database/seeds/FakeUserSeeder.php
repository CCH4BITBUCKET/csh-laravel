<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class FakeUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->name = 'JC';
        $user->email = 'jc@greenroom.com.my';
        $user->role_id = 1;
        $user->password = Hash::make('111111');
        $user->save();
//        factory(App\Models\User::class, 50)->create();
//        factory(App\Models\Product::class, 100)->create();
    }
}
