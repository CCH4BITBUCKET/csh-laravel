### Create Product ###

First of all, we would want to create a route for the product creation. Usually by creating a new page, we would need a new route, a new method in our controller and a display page (view).


* Create route in `routes/web.php`. `Route::get('product/create', 'ProductController@create');`

* Create `ProductController`, there's 2 ways to create this: Laravel command or the manual way, Laravel command will setup the controller nicely (namespacing and extending of `Controller`) where else if you do it the manual way, you have more manual setup that you need to work on yourself.

    - `php artisan make:controller ProductController` (recommended)
    - The manual way, right click and create file in `app/Http/Controllers` folder and save it as `ProductController.php`.
   
* Create the method `create()` in your controller, which will just display the production creation view.
  ```php
   public function create()
   {
      return view('backend.product.create');
   }
  ```
  
* Create the view at `resources/views/backend/product/create.blade.php`. Currently we only have 2 input: the product name and price.

```blade
<form action="{{ url('backend/product/create') }}" method="post">
    {{ csrf_field() }}
    <div class="form-group">
      <label for="name">Name</label>
      <input type="text" name="name" class="form-control" id="name" placeholder="Name" value="{{old('name')}}">
    </div>
    <div class="form-group">
      <label for="price">Price</label>
      <input type="text" name="price" class="form-control" id="price" placeholder="0.00"
             value="{{old('price')}}">
    </div>

    <button type="submit" class="btn btn-primary">Create</button>
</form>
```

* After creating the forms in the product creation page, we would need to construct the function to store it in the database. Let's start with creating the route and method in our controller.

web.php

`Route::post('product/create', 'ProductController@store');`

ProductController.php
```php
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name'  => 'required',
            'price' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $product = Product::create($request->only('name', 'price'));
        flash()->success($product->name . ' created');

        return redirect('backend/products');
    }
```

* If you have accessor set in your `App\Models\Product.php` model, remember to strip off the `$` and `,` before we insert it into the database with the mutator.

```php
    // Accessor
    public function getPriceAttribute($price)
    {
        return '$' . number_format($price, 2);
    }

    // Mutator
    public function setPriceAttribute($price)
    {
        $this->attributes['price'] = str_replace('$', '', $price);
        $this->attributes['price'] = str_replace(',', '', $this->attributes['price']);
    }
```

### Display All Product ###
* Create route

`Route::get('products', 'ProductController@index');`

* Create method in `ProductController`

```php
    public function index()
    {
        $products = Product::all();

        return view('backend.product.index', ['products' => $products]);
    }
```

* Create view `resources/views/backend/product/index.blade.php`. You can refer to the view in the bitbucket git repository. 

### Edit and Delete Product ###
* Create route at `routes/web.php`

`Route::get('product/{id}/edit', 'ProductController@edit');`

* Create method `edit()` in `ProductController.php`

```php
    public function edit($id)
    {
        try {
            $product = Product::findOrFail($id);
        } catch (Exception $e) {
            flash()->error('Product not found.');

            return redirect('backend/products');
        }

        return view('backend.product.edit', compact('product'));
    }
```

* Create the view `resources/views/backend/product/edit.blade.php`

```blade
@section('content')
  <h1 class="page-header">{{ $product->name }}</h1>
  <form id="form-delete" action="{{ url('backend/product/delete') }}" method="post">
    {{ csrf_field() }}
    <input name="id" class="hidden" value="{{$product->id}}">
  </form>
  <form action="{{ url('backend/product/update') }}" method="post">
    {{ csrf_field() }}
    <input name="id" class="hidden" value="{{$product->id}}">
    <div class="form-group">
      <label for="name">Name</label>
      <input type="text" name="name" class="form-control" id="name" placeholder="Name" value="{{$product->name}}">
    </div>
    <div class="form-group">
      <label for="price">Price</label>
      <input type="text" name="price" class="form-control" id="price" placeholder="0.00"
             value="{{$product->price}}">
    </div>

    <button type="submit" class="btn btn-primary">Update</button>
    <button id="btn-delete" type="button" class="btn btn-danger">Delete</button>
  </form>
@endsection
```

* Create the `update` route and method.

web.php
`Route::post('product/update', 'ProductController@update');`

ProductController.php
```php
    public function update(Request $request)
    {
        $product = Product::findOrFail($request->id);
        $product->update($request->only('name', 'price'));

        flash()->success('Product updated.');

        return redirect('backend/products');
    }
```

* Create the `delete` route and method.

web.php

`Route::post('product/delete', 'ProductController@delete');`

ProductController.php

```php
    public function delete(Request $request)
    {
        $product = Product::findOrFail($request->id);
        $product->delete();

        flash()->success('Product removed.');

        return redirect('backend/products');
    }
```