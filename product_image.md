### Product Image ###

We will add product images feature in the edit product page in this lesson.
There are multiple libraries (http://www.dropzonejs.com/, https://fineuploader.com/, https://blueimp.github.io/jQuery-File-Upload/) available, some requires a steep learning curve before we can start using it. 

In today's lesson, we will be using a very simple upload library, which is `http://hayageek.com/docs/jquery-upload-file.php`. 

Since this upload function will only be used in our product edit page, we can directly include the required js and css files in the page itself.

Open `resources/views/backend/product/edit.blade.php`

Add CSS file after `@extends('_layout.base-backend)`
```blade
@section('css')
  <link href="http://hayageek.github.io/jQuery-Upload-File/4.0.10/uploadfile.css" rel="stylesheet">
@endsection
```

Add Javascript file at the very bottom of the file
```blade
@section('js')
  <script src="http://hayageek.github.io/jQuery-Upload-File/4.0.10/jquery.uploadfile.min.js"></script>
@endsection
```

Add the upload `<div>`
```html
 <hr>
 <h4>Upload Picture</h4>
 <div id="fileuploader">+</div>
 <hr>
```

Add the javascript to initialise the upload plugin
```javascript
<script>
$(document).ready(function () {
  $("#fileuploader").uploadFile({
      acceptFiles: "image/*",
      uploadStr: "+",
      url: "{{url('backend/product/image/upload')}}",
      formData: {
          "_token": "{{csrf_token()}}",
          "product_id": "{{$product->id}}",
      },
      fileName: "image"
  });
});
</script>
```

Next we will need a route to handle the image upload. 

Add the below to `routes/web.php`.

`Route::post('product/image/upload', 'ProductController@upload');`

Open `ProductController.php` and add the below block of codes:
```php
    public function upload(Request $request)
    {
        $image = $request->image;
        $fileName = time() . "_" . $image->getClientOriginalName();

        //Store Image
        $image->storeAs('product_images', $fileName);

        //Thumbnail
        $thumbFileName = 'product_images/thumbnail/' . $fileName;
        $thumb = \Image::make($image->getRealPath())->resize(500, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $thumb->save($thumbFileName);

        //Save database
        $productImage = new ProductImage;
        $productImage->url = 'product_images/' . $fileName;
        $productImage->thumb_url = $thumbFileName;
        $productImage->product_id = $request->product_id;
        $productImage->save();

        return \Response::json([
            'product_id' => $productImage->id,
            'url'        => $productImage->url,
            'thumb_url'  => $productImage->thumb_url,
        ]);
    }
```
## Explanation: ##

 `$image = $request->image;`
 
We retrieve the image object with the above PHP code. Note that the image file name is the one you declared in the javascript `fileName: "image"` at your `edit.blade.php` page.

`$fileName = time() . "_" . $image->getClientOriginalName();`

Next, we create a new file name with the timestamp appended in front of the original file name.

`$image->storeAs('product_images', $fileName);`

We can call method `storeAs()` to physically save / store the file. The first parameter is the destination folder or path we will store the file and second parameter is the file name.
 
---
Next up, we will also want to create a thumbnail for the image uploaded. For this, we can utilise a package (http://image.intervention.io/) for image manipulation. Install the package by executing the below line in your terminal:

`composer require intervention/image`

As usual, when we use a 3rd party package, we would want to add the relevant service provider into `config/app.php`.

Add to `$providers` array:
`Intervention\Image\ImageServiceProvider::class`

Add to `$aliases` array:
`'Image' => Intervention\Image\Facades\Image::class`

Now we are all set to manipulate our uploaded image using the newly installed package.

```php
$thumbFileName = 'product_images/thumbnail/' . $fileName;
$thumb = \Image::make($image->getRealPath())->resize(500, null, function ($constraint) {
     $constraint->aspectRatio();
     $constraint->upsize();
 });
 $thumb->save($thumbFileName);
```
 
Notably here, `\Image::make()->resize()` is the line that performs the resizing and making of the thumbnail.
 
`make()` method will take in the image object.
 
`resize()` takes in 3 parameter: width, height and a callback(optional). In the callback, we can define some constraints on how we want the image to be resized.
 
Now if we try to upload a file, you should be able to see that it will be stored into `public/product_images` and the thumbnail will goes to `public/product_images/thumbnail/`.
 
Next, we would have to store the information of the image to the database, so that we can retrieve them and display later on.
 
Create a new migration for the `product_image` table.
 
Execute this in your terminal:
`php artisan make:migration Create_product_images_table`
 
Insert this into your newly created migration at `database/migrations`
```php
public function up()
{
     Schema::create('product_images', function (Blueprint $table) {
         $table->increments('id');
         $table->string('caption')->nullable();
         $table->string('url')->nullable();
         $table->string('thumb_url')->nullable();
         $table->integer('product_id')->unsigned();

         $table->foreign('product_id')->references('id')->on('products');
     });
}

/**
  * Reverse the migrations.
  *
  * @return void
*/
public function down()
{
     Schema::drop('product_images');
}
```
 
Execute `php artisan migrate` to create the new table.

Every table will have one model class that reference to the table. Create the model class.

Execute `php artisan make:model ProductImage` at your terminal. After which, move it to your `App\Models\` folder and update the namespace from `namespace App\` to `namespace App\Models;`

By default, when we call a `->save()` on a model class, Laravel will try to update the database field `created_at` and `updated_at`. In our product image table, we are not keeping track of those 2 fields, so we need to tell Laravel that. Open up `app\Models\ProductImage.php`, add `$timestamps = false`:

```php
class ProductImage extends Model
{
    public $timestamps = false;
}
```
 
Back to our explanations, in the `upload` method:
```php
$productImage = new ProductImage;
$productImage->url = 'product_images/' . $fileName;
$productImage->thumb_url = $thumbFileName;
$productImage->product_id = $request->product_id;
$productImage->save();
```

The above code will save the relevant information into our newly created database `product_images` table.

```php
return \Response::json([
    'product_id' => $productImage->id,
    'url'        => $productImage->url,
    'thumb_url'  => $productImage->thumb_url,
]);
```
Finally, we return the result in `json` format to the front end.

Now, let's modify our product's edit page to retrieve from database and display the new images that we've uploaded.

Add the block of code into
`resources/views/product/edit.blade.php` before `@endsection` of `content`

```blade
<div id="image-container">
    @foreach($product->images as $image)
      <div class="col-md-4" style="margin-bottom: 15px;">
        <img class="product-image" height="150" style="background: url('/{{$image->thumb_url}}');">
        <button id="{{$image->id}}" class="delete-button btn btn-danger">
          <span class="glyphicon glyphicon-trash"></span>
        </button>
      </div>
    @endforeach
    
    <div id="image-clone" class="col-md-4 hidden" style="margin-bottom: 15px;">
      <img class="product-image" height="150" style="background: url('/IMAGE_STUB');">
      <button id="IMAGE_ID_STUB" class="delete-button btn btn-danger">
        <span class="glyphicon glyphicon-trash"></span>
      </button>
    </div>
</div>
```

Also we need some custom CSS for the product image to be shown, append this in your `@section('css')`:

```blade
@section('css')
  <link href="http://hayageek.github.io/jQuery-Upload-File/4.0.10/uploadfile.css" rel="stylesheet">
  <style>
    .product-image {
      background-size: cover !important;
      background-position: center !important;
      width: 100%;
    }

    .delete-button {
      position: absolute;
      top: 0;
      right: 15px;
    }
  </style>
@endsection
```

Once it's done, hitting refresh on the page will show the images that you've uploaded. Now, one last thing to handle, is to display the image that we've uploaded without refreshing the page.

We can add in the `onSuccess()` function in our javascript `uploadFile()` function.
`onSuccess()` function will be triggered whenever our backend returns a result with a HTTP status of 200 (success).

Inside the `onSuccess()` function, we will use jQuery to manipulate the DOM and add the image to our `<div id="image-container">`

```javascript
$("#fileuploader").uploadFile({
      acceptFiles: "image/*",
      uploadStr: "+",
      url: "{{url('backend/product/image/upload')}}",
      formData: {
          "_token": "{{csrf_token()}}",
          "product_id": "{{$product->id}}",
      },
      fileName: "image",
      onSuccess: function (files, data, xhr) {
          var url = data.thumb_url;
          var productId = data.product_id;

          var newImage = $("#image-clone").clone().removeAttr("id").removeClass("hidden");
          newImage = newImage.prop("outerHTML");
          newImage = newImage.replace("IMAGE_STUB", url);
          newImage = newImage.replace("IMAGE_ID_STUB", productId);
          $("#image-container").append(newImage);
      }
});
```

Last part to wrap up this feature would be removal of uploaded images, when the user clicks on the trash bin icon on the right top of the image, ajax would be triggered to remove the image.

Create new route in `routes/web.php`:

`Route::post('product/image/delete', 'ProductController@imageDelete');`

Add the following javascript to `resources/views/backend/product/edit.blade.php`:
```javascript
// Delete images
$("#image-container").on("click", ".delete-button", function () {
  var toBeDeleteImage = $(this).parent();
  var formData = {
      "_token": "{{csrf_token()}}",
      "image_id": $(this).attr("id"),
  };

  $.ajax({
      url: "/backend/product/image/delete",
      type: "POST",
      data: formData,
      success: function (data, textStatus, jqXHR) {
          toBeDeleteImage.slideUp();
      },
      error: function (jqXHR, textStatus, errorThrown) {
          console.log(jqXHR.message);
      }
  });
});
```

Create the method `imageDelete()` in `ProductController.php`

```php
public function imageDelete(Request $request)
{
    $imageId = $request->image_id;
    try {
        $productImage = ProductImage::findOrFail($imageId);

        \Storage::delete($productImage->url);
        \Storage::delete($productImage->thumb_url);

        $productImage->delete();

        return \Response::json(['message' => 'success'], 200);
    } catch (Exception $e) {
        return \Response::json(['message' => 'Invalid ID'], 400);
    }
}
```

We retrieve the product image object from database and delete the physical file and thumbnail, and after that we remove the record from the database itself.