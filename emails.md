### Emails ###
Laravel comes with drivers to support sending emails over local or cloud based service.
- SMTP
- Mailgun
- SparkPost
- Amazon SES
- Mandrill
- sendmail

If you are using API based driver, you would need to install Guzzle HTTP library, which will be used to communicate with the APIs.

`composer require guzzlehttp/guzzle`

In this lesson, we will use MailTrap as our SMTP host. MailTrap provides a dummy inbox where we can view all our out outgoing emails, which is ideal for testing purposes. Head over to https://mailtrap.io and register an account.

Upon registering and login in, you will see the details as below:

```
SMTP
Host:	smtp.mailtrap.io
Port:	25 or 465 or 2525
Username:	xxx
Password:	xxxxx
Auth:	PLAIN, LOGIN and CRAM-MD5
TLS:	Optional
```

Open your Laravel `.env` file and fill in as below: 
```
MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=xxx
MAIL_PASSWORD=xxxxx
MAIL_ENCRYPTION=tls
```

Now we are ready to work on some code to send the email.

In Laravel, each type of email we want to send would be in their own `Mailable` class and stored in `app\Mail` directory.

In this lesson, we will create a simple enquiry form and send the enquiry through email.

---
Create a route for displaying the form in `routes/web.php`:

`Route::get('email', 'EmailController@index');`

---

Create the controller with `php artisan make:controller EmailController`.
Open the newly created controller and add the `index()` method.

```php
public function index()
{
    return view('backend.test_email.index');
}
```
---

Create the view `resources/views/backend/test_email/index.blade.php`:

```blade
@extends('_layout.base_backend')

@section('content')
  <form method="post" action="{{url('backend/test-email')}}">
    {{csrf_field()}}
    <textarea class="form-control" name="enquiry"></textarea>
    <br>
    <button type="submit" class="btn btn-primary">Send Email</button>
  </form>
@endsection
```

---

Create new route to submit the form to send the email in `routes/web.php`:

`Route::post('send-email', 'EmailController@send');`

---

Create the `send()` method:
```php
public function send(Request $request)
{
    $enquiry = $request->enquiry;

    \Mail::to('omegachien@gmail.com')
//            ->cc('jc@greenroom.com.my')
//            ->bcc('jc@mailinator.com')
        ->send(new ContactUsEmail($enquiry));

}
```
---
Now, let's create a new mailable by executing the command in your terminal:

`php artisan make:mail ContactUsEmail`

The new class `ContactUsEmail` will be placed in `app\Mail`. 

```php
class ContactUsEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $enquiry;
    
    /**
     * Create a new message instance.
     *
     * @param $enquiry
     */
    public function __construct($enquiry)
    {
        $this->enquiry = $enquiry;
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('serene@greenroom.com.my')
            ->subject('Enquiry for CSH')
            ->view('emails.contact_us');
    //            ->attach(public_path('product_images/1492053958_shoe-1.jpg'));
    }
}
```

We are passing the `$enquiry` over to the `ContactUsEmail` class. The view will have access to all the properties that have a `public` modifier in the mailable class.

Inside the `build()` method is where we can call various methods to config our email's presentation and delivery.

---

An email will consists of a view. Create the view at `resources/views/emails/contact_us.blade.php`.

You can find the content of the email view at  https://bitbucket.org/omegachien/csh-laravel .

Email view usually will consists of various inline CSSs to support and format the content nicely in various web browsers and email clients. A simple boilerplate to get started would be https://github.com/seanpowell/Email-Boilerplate 

By having a `public $enquiry` in your `Mailable` class, you can directly output `{{ $enquiry }}` in your view to access it automatically. 

---

You will notice that whenever we submit the enquiry form, it takes a few seconds until it completes the job. This is bad for user experience. We can utilise `Queue` to defer long processing tasks such as this.

Out of the box, Laravel supports a few queue backends:
- Beanstalk
- Amazon SQS
- Redis
- Relational database

Configuration file is at `config/queue.php`.

Let's use database as our queueing driver for this lesson.

Open `.env` file and modify `QUEUE_DRIVER`.

`QUEUE_DRIVER=database`

Next up, we will need to create the table that holds the queue jobs.

Execute the commands in terminal:
```
php artisan queue:table
php artisan migrate
```

A `jobs` table will be created.

In `EmailController`, instead of using the `send()` method, just change it to `queue()`.

```php
\Mail::to('omegachien@gmail.com')
//            ->cc('jc@greenroom.com.my')
//            ->bcc('jc@mailinator.com')
        ->queue(new ContactUsEmail($enquiry));
```

You will also need to run `php artisan queue:work` to process the jobs. In production, most likely you would want to setup a process monitor such as `supervisor` to automatically start `php artisan queue:work` in the background and restart it if it fails.

Sending attachments or embedding inline images is always a troublesome thing to work with while sending emails, but Laravel simplifies them.

Attachments:
```php
return $this->from('serene@greenroom.com.my')
            ->subject('Enquiry for CSH')
            ->view('emails.contact_us')
            ->attach(public_path('product_images/1492053958_shoe-1.jpg'));
```

Inline images:

```php
 <img src="{{ $message->embed(public_path('product_images/1491923794_Screen Shot 2017-03-28 at 15.45.49.png')) }}">
```
---

You can even queue the email to be sent later.
`$when` would be a `DateTime` instances.

```php
Mail::to($request->user())
    ->cc($moreUsers)
    ->bcc($evenMoreUsers)
    ->later($when, new OrderShipped($order));
```

